// import Maman from "./components/Maman"; Script Maman et enfant
import Catalog from "./components/Catalog";
import "./App.css";

function App() {
  /*return (
    <div className="App">
      <Maman />
    </div>
  );*/
  return (
    <div className="App">
      <Catalog />
    </div>
  );
}

export default App;
