import React, { Component } from "react";
import Enfant from "./Enfant";

class Maman extends Component {
  state = {
    messageMaman: null,
    messageEnfant: null,
  };

  handleSendOrder = () =>
    this.setState({ messageMaman: "Va ranger ta chambre" });

  handleChildResponse = () => {
    this.setState({ messageEnfant: "D'accord" });
  };

  render() {
    return (
      <>
        <h1>Maman</h1>
        <button onClick={this.handleSendOrder}>Envoyer ordre</button>
        <p>{this.state.messageMaman}</p>
        <hr />
        <Enfant childresponse={this.handleChildResponse} state={this.state} />
      </>
    );
  }
}

export default Maman;
