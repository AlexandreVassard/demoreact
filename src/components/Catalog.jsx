import React, { Component, Fragment } from "react";
import Car from "./Car";

class Catalog extends Component {
  state = {
    cars: [
      { name: "Audi RS6", color: "Rouge", year: 2010 },
      { name: "Megane RS", color: "Jaune", year: 2015 },
      { name: "Lamborghini Aventador", color: "Jaune", year: 2016 },
      { name: "Peugeot 206", color: "Bleue", year: 1999 },
    ],
  };

  handleAddYears = (years) => {
    const cars = this.state.cars.slice();
    cars.forEach((car) => {
      car.year = car.year - years;
    });
    this.setState({ cars });
  };

  render() {
    return (
      <>
        <h1>Catalogue des voitures</h1>
        <button onClick={this.handleAddYears.bind(this, 10)}>+10 ans</button>
        <button onClick={this.handleAddYears.bind(this, 20)}>+20 ans</button>
        {this.state.cars.map((car, index) => (
          <Fragment key={index}>
            <Car data={car} />
          </Fragment>
        ))}
      </>
    );
  }
}

export default Catalog;
