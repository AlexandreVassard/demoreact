import React from "react";

const Car = ({ data }) => {
  if (data.name && data.color && data.year && Number.isInteger(data.year)) {
    const year = new Date().getFullYear();
    const displayAge =
      year - data.year === 1 ? "1 an" : year - data.year + " ans";
    return (
      <>
        <div className="Car-div">
          <p>Nom : {data.name}</p>
          <p>Couleur : {data.color}</p>
          <p>Age : {displayAge}</p>
        </div>
      </>
    );
  } else {
    return null;
  }
};

export default Car;
