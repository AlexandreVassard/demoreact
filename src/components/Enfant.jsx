import React from "react";

const Enfant = ({ state, childresponse }) => {
  const responseButton = state.messageMaman ? (
    <button onClick={childresponse}>Répondre</button>
  ) : (
    <button disabled>Répondre</button>
  );
  return (
    <>
      <h1>Enfant</h1>
      {responseButton}
      <p>{state.messageEnfant}</p>
    </>
  );
};

export default Enfant;
